package es.esolvo.formacio;

import androidx.appcompat.app.AppCompatActivity;
import es.esolvo.formacio.classes.Accio;
import es.esolvo.formacio.classes.Empresa;
import es.esolvo.formacio.classes.Oportunitat;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView txvEmpresa;
    private TextView txvOportunitat;
    private TextView txvAccio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txvEmpresa = findViewById(R.id.empresa);
        txvOportunitat = findViewById(R.id.oportunitat);
        txvAccio = findViewById(R.id.accio);
        txvTipusAccio = findViewById(R.id.tipusAccio);

        Empresa empresa = new Empresa(1, "EmpresaProva", "Empresa de proves SLU", "987654321A");
        Oportunitat oportunitat = new Oportunitat(1, "Oportunitat de proves", new OportunitatCategoria(1,2,3,"HolaArtur.VullConflictirAmbTu"));
        TipusAccio tipusAccio = new TipusAccio(1, "NOM superGuai");
        Accio accio = new Accio(1, "The final action", tipusAccio);

        txvEmpresa.setText(empresa.toString());
        txvOportunitat.setText(oportunitat.toString());
        txvAccio.setText(accio.toString());
        txvTipusAccio.setText(tipusAccio.toString());
    }
}