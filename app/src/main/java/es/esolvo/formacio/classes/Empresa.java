package es.esolvo.formacio.classes;

public class Empresa {

    private int idEmpresa;
    private String nomEmpresa;
    private String nomFiscalEmpresa;
    private String nifEmpresa;

    public Empresa(int idEmpresa, String nomEmpresa, String nomFiscalEmpresa, String nifEmpresa) {
        this.idEmpresa = idEmpresa;
        this.nomEmpresa = nomEmpresa;
        this.nomFiscalEmpresa = nomFiscalEmpresa;
        this.nifEmpresa = nifEmpresa;
    }

    @Override
    public String toString() {
        return "Empresa{" +
                "idEmpresa=" + idEmpresa +
                ", nomEmpresa='" + nomEmpresa + '\'' +
                ", nomFiscalEmpresa='" + nomFiscalEmpresa + '\'' +
                ", nifEmpresa='" + nifEmpresa + '\'' +
                '}';
    }
}
