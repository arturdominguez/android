package es.esolvo.formacio.classes;

public class TipusAccio {

    private int idTipusAccio;
    private String nomTipusAccio;

    public TipusAccio(int idTipusAccio, String nomTipusAccio) {
        this.idTipusAccio = idTipusAccio;
        this.nomTipusAccio = nomTipusAccio;
    }

    @Override
    public String toString() {
        return "TipusAccio{" +
                "idTipusAccio=" + idTipusAccio +
                ", nomTipusAccio='" + nomTipusAccio + '\'' +
                '}';
    }
}
