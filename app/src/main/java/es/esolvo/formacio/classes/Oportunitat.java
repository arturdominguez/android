package es.esolvo.formacio.classes;

public class Oportunitat {

    private int idOportunitat;
    private String nomOportunitat;
    private OportunitatCategoria oportunitatCategoria;

    public Oportunitat(int idOportunitat, String nomOportunitat, OportunitatCategoria categoriaOportunitat) {
        this.idOportunitat = idOportunitat;
        this.nomOportunitat = nomOportunitat;
        this.categoriaOportunitat = categoriaOportunitat;
    }

    @Override
    public String toString() {
        return "Oportunitat{" +
                "idOportunitat=" + idOportunitat +
                ", nomOportunitat='" + nomOportunitat + '\'' +
                ", categoriaOportunitat='" + categoriaOportunitat.toString() + '\'' +
                '}';
    }
}
