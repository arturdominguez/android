package es.esolvo.formacio.classes;

public class Accio {

    private int idAccio;
    private String nomAccio;
    private TipusAccio tipusAccio;

    public Accio(int idAccio, String nomAccio, TipusAccio tipusAccio) {
        this.idAccio = idAccio;
        this.nomAccio = nomAccio;
        this.tipusAccio = tipusAccio;
    }

    @Override
    public String toString() {
        return "Accio{" +
                "idAccio=" + idAccio +
                ", nomAccio='" + nomAccio + '\'' +
                ", tipusAccio='" + tipusAccio.getNom() + '\'' +
                '}';
    }
}
