package es.esolvo.formacio.classes;

public class OportunitatCategoria {

    private int idOportunitatCategoria;
    private int idOportunitat;
    private int idCategoria;
    private String nomOportunitatCategoria;

    public OportunitatCategoria(int idOportunitatCategoria, int idOportunitat, int idCategoria, String nomOportunitatCategoria) {
        this.idOportunitatCategoria = idOportunitatCategoria;
        this.idOportunitat = idOportunitat;
        this.idCategoria = idCategoria;
        this.nomOportunitatCategoria = nomOportunitatCategoria;
    }

    @Override
    public String toString() {
        return "OportunitatCategoria{" +
                "idOportunitatCategoria=" + idOportunitatCategoria +
                ", idOportunitat='" + idOportunitat + '\'' +
                ", idCategoria='" + idCategoria + '\'' +
                ", nomOportunitatCategoria='" + nomOportunitatCategoria + '\'' +
                '}';
    }
}
